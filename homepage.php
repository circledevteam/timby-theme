<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package timby
 * Template Name: Homepage    
 */
 get_header(); 
 
 ?>

	
<!-- section -->
<section role="main" class="row-big">
	<section class=" relative">
    <img src="<?php echo get_template_directory_uri(); ?>/images/new-top.jpg" class="l-group home-image" id="homeImage">
    <h2 class="text-darkgreen text-center home-text" id="homeText">We are grassroots activists using technology to hold the Liberian government &amp; corporations to their commitments</h2>
  </section>

  <div class="line-interstitial"></div>

  <section class="findings">
    <header class="clearfix l-group-big">
      <h3 class="text-center text-orange text-light">Our Investigations</h3>
      <div class="six shift-three text-center columns">
        <p class="l-group-small">Our team writes long form, investigative stories based on our reporting and experiences.</p>
        <div class="clearfix">
          <a href="stories" class="btn btn-simple btn-orange btn-center btn-big" style="margin-top: 7px" >See All Investigations</a>
        </div>
      </div>
    </header>
    <?php 
    if( count($stories = fetch_published_stories()) > 0) { ?>
      <div class="row-big">
      <?php  foreach($stories as $story) { 
        ?>
          <div class=" story-home-left">
            <?php if ($story->featured_image){ ?>
              <a href="<?php echo esc_url(home_url('/')) ?>/story/?id=<?php echo $story->id ?>" class="clearfix">
                <img src="<?php echo $story->featured_image ?>" alt="Story photo" >
              </a>
            <?php } ?>
          </div>
          <div class=" story-home-right">
            <a href="<?php echo esc_url(home_url('/')) ?>/story/?id=<?php echo $story->id ?>" class="clearfix">
              <div class="l-group">
                <h4 class="text-darkgreen"><?php echo $story->title ?></h4>
                <h6 class="subhead"><?php echo $story->created; ?></h6>
              </div>
              <p class="margin-bottom l-group"><?php echo $story->sub_title ?></p>
              <a href="<?php echo esc_url(home_url('/')) ?>/story/?id=<?php echo $story->id?>" class="btn btn-darkgreen btn-full-width">Read Investigation</button>
            </a>
          </div>
      <?php break; } ?>
    </div>
    <?php } //end of loop ?>
    </div>
  </section>
</section>

  <div class="line-interstitial"></div>

  <section class="how-we-do-it row-big">
    <header class="l-group-big">
      <h3 class="text-center text-orange text-light">How we do it</h3>
      <div class="six shift-three text-center columns">
        <p class="l-group-small">Our on-the-ground team uses the TIMBY (This is My Backyard) android app, moderation platform and reporting tool to get information from the forests of Liberia out to the world.</p>
        <div class="clearfix">
          <a href="http://timby.org" class="btn btn-simple btn-orange btn-center btn-big" style="margin-top: 7px" >Learn more about TIMBY</a>
        </div>
      </div>
    </header>
    <div class="row-big ">
      <div class="four columns">
        <img src="<?php echo get_template_directory_uri(); ?>/images/reporting-front.jpg" class="l-group" style="width: 60%; margin: 0 auto;display: block;margin-bottom: 2em;">
        <h5 class="text-center text-darkgreen all-caps">Monitor</h5>
        <p class="p-sans text-center p-limited">Our TIMBY app takes geo-referenced photos, videos and audio recordings in the field</p>
      </div>
      <div class="four columns">
        <img src="<?php echo get_template_directory_uri(); ?>/images/dashboard-front.jpg" class="l-group how-we-do-it"  style="width: 60%; margin: 0 auto;display: block;margin-bottom: 2em;">
        <h5 class="text-center text-darkgreen all-caps">Moderate</h5>
        <p class="p-sans text-center p-limited">We moderate the reports, verifying and categorizing information for later investigation</p>
      </div>
      <div class="four columns">
        <img src="<?php echo get_template_directory_uri(); ?>/images/story-front.jpg" class="l-group how-we-do-it" style="width: 60%; margin: 0 auto;display: block;margin-bottom: 2em;">
        <h5 class="text-center text-darkgreen all-caps">Report</h5>
        <p class="p-sans text-center p-limited">We link together the media reports into stories that we share with the world.</p>
      </div>
    </div>
  </section>

   <div class="line-interstitial"></div>


<?php get_footer(); ?>
