<footer>
  

  <div class="row-big">
    <div class="five columns">
      <header class="">
        <h5 class="text-darkgreen margin-bottom all-caps ">Supported By</h5>
      </header>
      <div class="clearfix l-group">
        <div class="four  preserve">
          <a href="https://tribecafilminstitute.org/"><img src="<?php echo get_template_directory_uri(); ?>/images/tfi.png"></a>
        </div>
        <div class="four  preserve">
          <a href="http://www.makingallvoicescount.org/"><img src="<?php echo get_template_directory_uri(); ?>/images/mavc.png"></a>
        </div>
        <div class="four  preserve">
          <a href="http://africannewschallenge.org/"><img src="<?php echo get_template_directory_uri(); ?>/images/anic.png"></a>
        </div>
      </div>
      <div class="clearfix  l-group">
        <div class="four  preserve">
          <a href="http://africanmediainitiative.org/"><img src="<?php echo get_template_directory_uri(); ?>/images/ami.png"></a>
        </div>
        <div class="four  preserve">
          <a href="http://www.goldmanfund.org/"><img src="<?php echo get_template_directory_uri(); ?>/images/goldman.png"></a>
        </div>
      </div>
      <p><small>© TIMBY Productions Inc. All rights reserved. </small></p>
    </div>

    <div class="seven columns">
      <div class="clearfix l-group" style="margin-bottom: 2em">
        <header class="">
          <h5 class="text-darkgreen margin-bottom all-caps  l-group">Our Newsletter</h5>
        </header>
        <div id="mc_embed_signup">
          <form action="//timby.us10.list-manage.com/subscribe/post?u=f3038c45ad993b663adc6a3de&amp;id=22db556677" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">
              <div class="mc-field-group clearfix">
                <!-- <label for="mce-EMAIL">Email Address </label> -->
                <div class="nine columns">
                  <input type="email" value="" name="EMAIL" class="required email input-full-width input-big" id="mce-EMAIL" style="border-color: white" placeholder="Enter your e-mail address">
                </div>
                <div class="three columns">
                  <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-full-width btn-darkgreen btn-big">
                </div>
              </div>
              <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
              </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;"><input type="text" name="b_f3038c45ad993b663adc6a3de_22db556677" tabindex="-1" value=""></div>
              </div>
          </form>
        </div>
      </div>

      <div class="clearfix">
        <div class="clearfix">
          <div class="six columns">
            <header class="">
              <h5 class="text-darkgreen margin-bottom all-caps  l-group">Contact SDI Liberia</h5>
            </header>
            <div class="clearfix">
              <div class="seven preserve">
                <a href="mailto:timby@sdiliberia.org"><span class="fa fa-envelope"></span> timby@sdiliberia.org</a><br/>
                <a href="http://sdiliberia.org"><span class="fa fa-link"></span> sdiliberia.org</a>
              </div>
              <div class="two preserve text-center">
                <a href="https://www.facebook.com/sdi.liberia"><span class="fa fa-facebook-square fa-4"></span></a>
              </div>
              <div class="two preserve text-center">
                <a href="http://twitter.com/sdiliberia"><span class="fa fa-twitter-square fa-4"></span></a>
              </div>
            </div>
          </div>
          <div class="six columns">
            <header class="">
              <h5 class="text-darkgreen margin-bottom all-caps l-group">Contact TIMBY</h5>
            </header>
            <div class="clearfix">
              <div class="seven preserve">
                <a href="mailto:info@timby.org"><span class="fa fa-envelope"></span> info@timby.org</a><br/>
                <a href="http://timby.org"><span class="fa fa-link"></span> timby.org</a>
              </div>
              <div class="two preserve text-center">
                <a href="https://www.facebook.com/profile.php?id=100007822067128&fref=ts"><span class="fa fa-facebook-square fa-4"></span></a>
              </div>
              <div class="two preserve text-center">
                <a href="http://twitter.com/timbyafrica"><span class="fa fa-twitter-square fa-4"></span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

<script type="text/javascript" href="<?php echo get_template_directory_uri(); ?>/js/frontend.js"></script>

		<?php wp_footer(); ?>
  
<script type="text/javascript">
  $( "#embed_button").click(function() {
    $("#embed").toggle();
  });
  function centerImage(){
    var imageWidth = $('#homeImage').width();
    var imageHeight = $('#homeImage').height();
    var textWidth = $('#homeText').innerWidth();
    var textHeight = $('#homeText').innerHeight();
    $('#homeText').css('margin-left', (textWidth / 2)*-1);
    $('#homeText').css('margin-top', (textHeight / 1.8)*-1);
  }
function equalHeight(){
  var leftH = $('.story-home-left img').innerHeight();
  $('.story-home-right').height(leftH);
  console.log('equal');
}
$(document).ready(function(){
  
  var mql = window.matchMedia('all and (min-width: 480px)');
  if (mql.matches) {
      centerImage();
  }
  var imql = window.matchMedia('all and (min-width: 769px)');
  if (imql.matches) {
      equalHeight();
  }
  $('#mainMenu').click(function(event) {
    event.preventDefault();
    $('.menu').slideToggle();
    $('#mainMenuClose').toggle();
    $(this).slideToggle();
  });
  $('#mainMenuClose').click(function(event) {
    event.preventDefault();
    $('.menu').slideToggle();
    $('#mainMenu').slideToggle();
    $(this).toggle();
  });

});

$(window).resize(function(){
  var mql = window.matchMedia('all and (min-width: 480px)');
  if (mql.matches) {
      equalHeight();
  }
  var imql = window.matchMedia('all and (min-width: 769px)');
  if (imql.matches) {
      equalHeight();
  }
});
</script>

	</body>
</html>
