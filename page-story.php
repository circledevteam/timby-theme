<?php
/**
 * The template for displaying a single story
 *
 *
 * @package timby
 */
get_header();
$story_id = (int) sanitize_text_field($_REQUEST['id']);

global $wpdb;
$tablename = $wpdb->prefix . 'published_stories';

$story = $wpdb->get_row("SELECT id, title,  sub_title, content, featured_image FROM $tablename WHERE id = $story_id");
$story = build_story_data($story);

// parse the json story content string
$story->content = json_decode($story->content);
?>
<!-- section -->
<section role="main" class="row-big l-group-big">
  <div class="l-group text-center">
    <a href="<?php echo bloginfo('url');?>/stories" class="btn btn-small btn-orange"><span class="fa fa-angle-left"></span> Back to Story List</a>
  </div>
  <div class="header-group l-group text-center ten shift-one">
    <h1><?php echo $story->title ?></h1>
    <h3><?php echo $story->sub_title ?></h3>
  </div>
  <div class="l-group twelve">
    <img src="<?php echo $story->featured_image; ?>" />
  </div>
  <div class="text-center">
    <img src="<?php echo get_template_directory_uri(); ?>/images/splitter.jpg" alt="" >
  </div>
  <div class="content-blocks l-group eight shift-two">
    <?php foreach ($story->content as $key => $content) { ?>
      <?php if( $content->type == 'editor') { ?>
        <p><?php echo $content->text ?></p>
      <?php } ?>

      <?php if( $content->type == 'report') { ?>
        <div class="l-group l-group-spaced">
          <div class="report-thumb frontend">
            <div class="report-flag">
              <span class="fa fa-eye"></span>
              Primary<br/>Source
            </div>

            <div class="report-thumb-content clearfix">
              <div class="three report-thumb-map">
                <div style="width: 100%; height: 200px;" class="timby-thumb-map" data-lat="<?php echo $content->report->lat ?>"  data-lng="<?php echo $content->report->lng ?>" ></div>
              </div>

              <div class="nine report-thumb-info">
                <h4 class="list-title all-caps">
                  <?php echo $content->report->post_title ?>
                </h4>
                <span class="list-details text-muted">
                    <?php echo $content->report->date_reported ?>
                  </span>
                <div class="list-content">
                  <div class="list-content-description">
                    <?php echo $content->report->post_content ?>
                  </div>
                </div>
                <!-- <div class="report-thumb-media">
                  <?php foreach($content->report->media->photos as $photo) { ?>
                    <div class="three">
                      <a href="">
                        <img src="<?php echo $photo->small ?>">
                      </a>
                    </div>

                  <?php } ?>
                  <?php foreach($content->report->media->audio as $audio) { ?>
                    <div class="three">
                      <iframe src="<?php echo $audio->soundcloud->embed_url ?>" width="100%"  height="120" scrolling="no" frameborder="no"></iframe>
                    </div>
                  <?php } ?>

                  <?php foreach($content->report->media->video as $video) { ?>
                    <div class="three">
                      <iframe
                          src="<?php echo $video->vimeo->embed_url ?>"
                          frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen>
                      </iframe>
                    </div>
                  <?php } ?>
                </div> -->
                <a href="" class="btn btn-darkgreen basic" data-key="<?php echo $key?>" id="view-modal" >
                  View full report
                </a>
                <!-- <div class="report-tools">
                  <a class="btn embed_button">Embed</a>
                  <a class="btn"  href="<?php echo $content->report->download_link ?>">Download</a>
                  <div class="embed"  style="display: none;">
                    <h5>Embed this report</h5>
                    <textarea name="" id="" style="width:100%; height:80px;" ><?php echo $content->report->embed_code ?></textarea>
                  </div>
                </div> -->
              </div>

            </div>

          </div>
        </div>
        <div id='container'>
          <div id='content'>
            <!-- modal content -->

            <div id="basic-modal-content" <?php echo 'class="'.$key.'"';?>>
              <div class="report-flag"  >
                <span class="fa fa-eye"></span>
                Primary<br/>Source
              </div>
              <h3><?php print_r($content->report->post_title);?></h3>




              <div class="list-content">
                  <span class="list-details text-muted">
                    <?php echo $content->report->date_reported ?>
                  </span>
                <div class="list-content-description">
                  <?php echo $content->report->post_content ?>
                </div>
              </div>
               <div class="report-thumb-media">
                  <?php foreach($content->report->media->photos as $photo) { ?>
                    <div class="twelve">
                      <a href="">
                        <img src="<?php echo $photo->large ?>">
                      </a>
                    </div>
                      <?php echo "$photo->post_excerpt";?>
                  <?php } ?>
                  <?php foreach($content->report->media->audio as $audio) { ?>
                    <div class="twelve">
                      <iframe src="<?php echo $audio->soundcloud->embed_url ?>" width="100%"  height="120" scrolling="no" frameborder="no"></iframe>
                    </div>
                      <?php echo "$audio->post_excerpt";?>
                  <?php } ?>

                  <?php foreach($content->report->media->video as $video) { ?>
                    <div class="twelve">
                      <iframe
                          src="<?php echo $video->vimeo->embed_url ?>"
                          frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen>
                      </iframe>
                    </div>
                      <?php echo "$video->post_excerpt";?>
                  <?php } ?>

            </div>
            <!-- <div class="clearfix">
              <div class="three report-thumb-map">
                <div style="width: 170px; height: 200px;" class="timby-thumb-map" data-lat="<?php echo $content->report->lat ?>"  data-lng="<?php echo $content->report->lng ?>" ></div>
              </div>
            </div> -->
            <div class="clearfix">
              <div class="clearfix l-group">
                <a href="<?php echo $content->report->download_link; ?>" class="btn btn-orange pull-left">Download</a>
                <a href="" class="btn btn-darkgreen embed_button pull-right">Embed</a>
              </div>
              <div class="embed" hidden >
                <h5>Embed this report</h5>
                <textarea name="" id="" style="width:100%; height:80px;" ><?php echo $content->report->embed_code ?></textarea>
              </div>
            </div>

            <!-- preload the images -->
            <div style='display:none'>
              <img src='img/basic/x.png' alt='' />
            </div>
          </div>

        </div>

      <?php } ?>
    <?php } ?>
  </div>

</section>
<!-- /section -->


<?php get_footer(); ?>
