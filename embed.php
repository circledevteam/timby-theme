<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package timby
 * Template Name: Embed
 */


?>
<?php
require_once __DIR__ . '/../../../wp-load.php';


$reportid = (int) $_GET['id'];
$args = array(
  'post__in' => array($reportid),
  'post_type'   => 'report',
  'post_status' => 'publish',
  'meta_query' => array(
    array(
      'key'   => '_report_status',
      'value' => 'public'
    ),
  )
);
$report = get_posts($args);
$report = $report[0];

if( empty($report)) exit('Sorry that report was either not found or removed!');
$report = build_report_data($report)
?>
<!DOCTYPE html>
<html lang="en" ng-app="timby">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <style>
<<<<<<< HEAD
	    	.cf:before,
	.cf:after {
	    content: " "; /* 1 */
	    display: table; /* 2 */
	}
	
	.cf:after {
	    clear: both;
	}
	
	.cf {
	    *zoom: 1;
	}
=======
        .cf:before,
  .cf:after {
      content: " "; /* 1 */
      display: table; /* 2 */
  }
  
  .cf:after {
      clear: both;
  }
  
  .cf {
      *zoom: 1;
  }
>>>>>>> 102db1100ba3186e614477365210bdea1fb29d67
      body {
        font-family: helvetica, sans-serif;
      }

      .report-thumb {
          border: 1px solid #ccc;
          *zoom: 1;
 
      }
      .report-thumb-content {
<<<<<<< HEAD
	      line-height: 24px;
	      padding: 14px;
      }

      p {
	      margin-top: 0;
	      color:#444;
=======
        line-height: 24px;
        padding: 14px;
      }

      p {
        margin-top: 0;
        color:#444;
>>>>>>> 102db1100ba3186e614477365210bdea1fb29d67
      }

      #report-location {
          width: 100%;
          height: 150px
      }
      
      .report-thumb-header {
<<<<<<< HEAD
	      background: #efefef;
	      padding: 10px;
      }
      .report-thumb-photo{
	      float:left;
	      margin-right: 1em
      }
            .report-thumb-info {
	            float:left;
	            margin-top: 12px;
            }
      h4 {
	      margin-top: 0;
	      font-weight: 100;
	      color: #666;
      }
      h3 {
	      margin: 0 0 5px 0;
      }
		.vert-align {
		  position: relative;
		  top: 50%;
		  transform: translateY(-50%);
		}
		.twelve img {
			width: 100%
		}
=======
        background: #efefef;
        padding: 10px;
      }
      .report-thumb-photo{
        float:left;
        margin-right: 1em
      }
            .report-thumb-info {
              float:left;
              margin-top: 12px;
            }
      h4 {
        margin-top: 0;
        font-weight: 100;
        color: #666;
      }
      h3 {
        margin: 0 0 5px 0;
      }
    .vert-align {
      position: relative;
      top: 50%;
      transform: translateY(-50%);
    }
    .twelve img {
      width: 100%
    }
>>>>>>> 102db1100ba3186e614477365210bdea1fb29d67
    </style>
    
    <script>
      // google analytics event tracking
      if( _gaq )
         _gaq.push(['_trackEvent', 'Reports - Embed', 'Viewed', '<?php echo $report->post_title ?>']);

    </script>
    <script>
      // passed as a callback after the maps script loads
      function initialize(){
        var _map_element = document.getElementById('report-location');

        // initialize the map
        var map = new google.maps.Map(
          _map_element,
          {
            zoom: 7,
            center: new google.maps.LatLng(
              _map_element.getAttribute('data-lat'),
              _map_element.getAttribute('data-lng')
            )
          }
        );
        // add marker
        new google.maps.Marker({
          position: new google.maps.LatLng(
            _map_element.getAttribute('data-lat'),
            _map_element.getAttribute('data-lng')
          ),
          map: map
        });
      }

      // load the maps library if its not yet loaded
      function loadScript(){
        if( !window.hasOwnProperty('google') || !window.google.hasOwnProperty('maps') ){
          var gm = document.createElement('script');
          gm.type = 'text/javascript';
          gm.src = "//maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&callback=initialize";
          document.body.appendChild(gm);
        }        
      }
      window.onload = loadScript;
    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/iframeResizer.contentWindow.js"></script>
  </head>
  <body>
    <div class="report-thumb">
<<<<<<< HEAD
	<div class="report-thumb-header cf">	
		<div class="report-thumb-photo">
			<img src="<?php echo get_template_directory_uri(); ?>/images/sdi-logo.png" alt="Logo" width="100">
		</div>
	  <div class="report-thumb-info ">
		    <h3 class="list-title"><?php echo $report->post_title ?></h3>
		    <h4><?php echo $report->date_reported  ?></h4>
	  </div>
=======
  <div class="report-thumb-header cf">  
    <div class="report-thumb-photo">
      <img src="<?php echo get_template_directory_uri(); ?>/images/sdi-logo.png" alt="Logo" width="100">
    </div>
    <div class="report-thumb-info ">
        <h3 class="list-title"><?php echo $report->post_title ?></h3>
        <h4><?php echo $report->date_reported  ?></h4>
    </div>
>>>>>>> 102db1100ba3186e614477365210bdea1fb29d67
    </div>
    
    <div class="report-thumb-content">
       
        <?php echo $report->post_content; ?>

          <div class="report-thumb-media">
              <?php foreach($report->media->photos as $photo) { ?>
                  <div class="twelve">
                      <img src="<?php echo $photo->large ?>">
                  </div>
                 <h4><?php echo $photo->post_excerpt;?></h4>
              <?php } ?>
              <?php if (!empty($report->media->audio)) { foreach($report->media->audio as $audio) { ?>
<<<<<<< HEAD
                  <div class="three audio">
=======
                  <div class="twelve">
>>>>>>> 102db1100ba3186e614477365210bdea1fb29d67
                      <iframe src="<?php echo $audio->soundcloud->embed_url ?>" width="100%"  height="120" scrolling="no" frameborder="no"></iframe>
                  </div>
                  <?php echo $audio->post_excerpt;?>
              <?php }}?>

              <?php  if (!empty($report->media->video)) { foreach($report->media->video as $video) { ?>
<<<<<<< HEAD
                  <div class="three video">
=======
                  <div class="twelve">
>>>>>>> 102db1100ba3186e614477365210bdea1fb29d67
                      <iframe
                          src="<?php echo 'https://' . $video->vimeo["embed_url"]; ?>"
                          frameborder="0" width="100%" webkitallowfullscreen mozallowfullscreen allowfullscreen>
                      </iframe>
                  </div>
                  <?php echo $video->post_excerpt;?>
              <?php }} ?>
          </div>
      </div>

      <div class="five report-thumb-map">
        <div id="report-location" data-lat="<?php echo $report->lat ?>" data-lng="<?php echo $report->lng ?>" ></div>
    </div>
    </div>
  </body>
</html>
