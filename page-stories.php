<?php
/**
 * The template for displaying stories
 *
 *
 * @package timby
 */
 get_header(); ?>
  
<!-- section -->
<section role="main" class="row-big l-group-big">
  
  <header class="l-group-big">
    <h1><?php the_title(); ?></h1>
    <p class="p-big s-sans eight shift-two text-center">
      Our investigations are collections of on-the-ground reports and after the fact writings that give a chronological account of a developing situation.
        <?php while (have_posts()) : the_post(); ?>
          <?php $content = $post -> post_content;
                echo $content;
           ?>
        <?php endwhile; //end of loop ?>
    </p>
  </header>
  
  <?php if( count($stories = fetch_published_stories()) > 0) { ?>
    <div class="row-big">
    <?php foreach($stories as $story) { 
      ?>
        <a href="<?php echo esc_url(home_url('/')) ?>/story/?id=<?php echo $story->id ?>" class="four columns story-list">
          <figure class="story-list-figure">
            <img src="<?php echo $story->featured_image; ?>" />
          </figure>
          <div class="grid-item-top grid-item-loose">
            <div class="l-group">
              <h4 class="text-darkgreen"><?php echo $story->title ?></h4>
              <h6 class="subhead"><?php echo $story->created ?></h6>
            </div>
            <p class="p-sans"><?php echo $story->sub_title ?></p>
          </div>
        </a>
    <?php } ?>
  </div>
  <?php } //end of loop ?>
</section>
<!-- /section -->

<?php get_footer(); ?>
