module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),



        // imagemin: {
        //     dynamic: {
        //         files: [{
        //             expand: true,
        //             cwd: 'cms/wp-content/themes/timby/images/',
        //             src: ['*.{png,jpg,gif}'],
        //             dest: 'cms/wp-content/themes/timby/images/build/'
        //         }]
        //     }
        // },

        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'css/global.css': 'sass/global.sass',
                    '../../plugins/timby-plugin/assets/css/global.css': 'sass/global.sass',
                    'css/frontend.css': 'sass/frontend.sass'
                    // 'cms/wp-content/themes/timby/css/bootstrap.css': 'cms/wp-content/themes/timby/bower_components/bootstrap-sass/vendor/assets/stylesheets/bootstrap.scss'
                }
            }
        },

        watch: {
            // options: {
            //     livereload: true,
            // },
            
            css: {
                files: ['sass/*','sass/modules/*','sass/modules-front/*','sass/layout/*'],
                tasks: ['sass'],
                options: {
                    spawn: false,
                }
            }
        }

    });

    // 3. Where we tell Grunt we plan to use plug-ins.
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['sass','watch']);

};
